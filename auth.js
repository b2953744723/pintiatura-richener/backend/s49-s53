const jwt = require('jsonwebtoken');


const secret = "ECommerceAPI";


// Token Creation
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	
	return jwt.sign(data,secret, {});
};


// Token Verification

module.exports.verify = (req, res, next) => {
	console.log(req.header.authorization);

	let token = req.headers.authorization;

	if(typeof token === "undefined") {
		return res.send({auth: "Failed. No Token received."});
	} else {

		token = token.slice(7, token.length);
	}

	// Token decryption
	jwt.verify(token, secret, function(err, decodedToken) {
		if(err) {
			return res.send({
				auth: "Failed",
				message: err.message
			});

		} else {
			req.user = decodedToken;
			next();
		}
	})
};

// verifyAdmin functioni
module.exports.verifyAdmin = (req, res, next) => {

	console.log(req.user);
	if(req.user.isAdmin) {
		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}