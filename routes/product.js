// Dependencies and Modules
const express = require('express');
const auth = require('../auth');
const productController = require('../controllers/product');

const { verify, verifyAdmin } = auth;

const router = express.Router();


// Route for creating a product
router.post("/createProduct", verify, verifyAdmin, productController.addProduct);

// Route for retrieve all products (ADMIN)
router.get("/all", productController.getAllProducts);

// Route for retrieving all active product
router.get("/allActive", productController.getAllActiveProducts);

// Roupte for retrieving a single roduct
router.get("/:productId", productController.getSingleProduct);

// Route for updating a product (ADMIN)
router.put("/:productId", verify ,verifyAdmin, productController.updateProduct);

// Route for archiving a product (ADMIN)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// Route for activating a product (ADMIN)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

// Route to search for products by product name
router.post('/search', productController.searchProductsByName);



module.exports = router;