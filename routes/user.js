// Dependencies and Modules
const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');
const User = require("../models/User");
const Order = require("../models/Order");

// Destructure the auth file:
const { verify, verifyAdmin } = auth;

const router = express.Router();


// Route for checking if user email already exist.
router.post("/checkEmail", userController.checkEmailExists);

// Route for user registration
router.post("/register", userController.registerUser);

// Route for login user
router.post("/login", userController.loginUser);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);

// Route for resetting the password
router.put('/reset-password', verify, userController.resetPassword);

// Route for retrieving user details
router.get("/details", verify, userController.getDetail);

// Route for user to retrieve all orders
// router.get("/getUserOrders", verify, userController.getUserHistory);

// Route for set user to as admin
router.put("/updateAdmin", verify, verifyAdmin, userController.updateUserAsAdmin);

// Route for user to add cart
router.post("/addToCart", verify, userController.addToCart);


module.exports = router;