// Dependencies and Modules
const express = require('express');
const cartController = require('../controllers/cart');
const auth = require('../auth');

// Destructure the auth file:
const { verify, verifyAdmin } = auth;

const router = express.Router();


// Route for user to add cart
router.post("/addToCart", verify, cartController.addToCart);


module.exports = router;