// Dependencies and Modules
const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');

// Destructure the auth file:
const { verify, verifyAdmin } = auth;

const router = express.Router();


// Route for user to order a product
router.post("/createOrder", verify, orderController.createOrder);

// Route for user to retrieve all orders
router.get("/getOrders",verify, orderController.getUserOrders);

// Route for retrieve all orders (ADMIN)
router.get("/getAllOrders", verify, verifyAdmin, orderController.getAllOrders);


module.exports = router;