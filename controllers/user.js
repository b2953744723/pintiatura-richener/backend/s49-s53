// Dependencies and Modules
const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require("../auth");
const Order = require("../models/Order");
const Product = require("../models/Product");
const Cart = require("../models/Cart");

// Controllers to check email
module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {
		console.log(result)
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
	.catch(err =>{
		console.log(err)
		res.send(err)} );

};

// Controllers to register a user
module.exports.registerUser = (req, res) => {

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: bcrypt.hashSync(req.body.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return res.send(false)
		} else {
			return res.send(true)
		}
	}).catch(err => err)
};


// Controllers to login a user
module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => { 

		if(result == null) {
			return res.send(false)
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if(isPasswordCorrect) {
				return res.send({access: auth.createAccessToken(result)})
			} else {
				return res.send(false)
			}
		}
	}).catch(err => res.send(err));
};


// Controllers for getting a User details
module.exports.getDetail = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = ""
		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Controllers for a user to as admin
module.exports.updateUserAsAdmin = (req, res) => {
	let updateAdmin = {
		userId: req.body.id,
		isAdmin: true
	}

	return User.findByIdAndUpdate(req.body.userId, updateAdmin).then((user, error) => {
		if(error) {
			return res.send(false)
		}
		else if (user.isAdmin == true) {
			return res.send(false)
		}
		 else {
			return res.send(true)
		} 
	})
	.catch(err => res.send(err))
};


module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
};


module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json(true);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

// module.exports.getUserOrders = async (req, res) => {
//   try {
//     const userId = req.user.id;

//     // Assuming you have a "User" model to check if the user exists
//     const userExists = await User.findById(userId);
//     if (!userExists) {
//       return res.status(404).json({ message: 'User not found.' });
//     }

//     // Fetch orders for the specific user
//     const orders = await Order.find({ userId }).populate('products.productId', 'name');

//     return res.json(orders);
//   } catch (error) {
//     console.error('Error fetching user orders:', error);
//     return res.status(500).json({ message: 'An error occurred while fetching user orders.' });
//   }
// };


module.exports.addToCart = async (req, res) => {
  try {
    const { productId, quantity } = req.body;
    const userId = req.user.id;

    const product = await Product.findById(productId);
    if (!product) {
      return res.send(false);
    }

    const price = product.price;
    const totalAmount = price * quantity;

    // Check if the user already has a cart
    let cart = await Cart.findOne({ userId });

    if (!cart) {
      // If the user doesn't have a cart, create a new one
      cart = new Cart({
        userId,
        products: [{ productId, quantity, price }],
        totalAmount,
      });
    } else {
      // If the user already has a cart, update it
      const existingProduct = cart.products.find(
        (product) => product.productId === productId
      );

      if (existingProduct) {
        // If the product already exists in the cart, update its quantity
        existingProduct.quantity = quantity;
        existingProduct.subtotal = price * quantity;
      } else {
        // If the product is new, add it to the cart
        cart.products.push({ productId, quantity, price });
      }

      cart.totalAmount = cart.products.reduce(
        (total, product) => total + product.subtotal,
        0
      );
    }

    await cart.save();

    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.send(false);
  }
};








