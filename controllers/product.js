// Dependencies and Modules
const Product = require("../models/Product");
const auth = require('../auth');
const User = require('../models/User');


// Controllers for adding a product (ADMIN)
module.exports.addProduct = (req, res) => {

	let newProduct = new Product ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	return newProduct.save().then((product, error) => {
		if(error) {
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err));

};


// Controllers for getting all of a product
module.exports.getAllProducts = (req, res) => {
	return Product.find({}).then(result => {
		return res.send(result);
	})
	.catch(err => res.send(err));
};


// Controllers for getting all ACTIVE product
module.exports.getAllActiveProducts = (req, res) => {
	return Product.find({isActive: true}).then(result => {
		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Controllers for getting a single product
module.exports.getSingleProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result => {
		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Controllers for updating a product (ADMIN)
module.exports.updateProduct = (req, res) => {

	let updatedProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {
		if(error) {
			return res.send(false)
		} else {
			return res.send(true)
		}
	}) 
	.catch(err => res.send(err))
};


// Controllers for archiving a product
module.exports.archiveProduct = (req, res) => {
	let archivedProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(req.params.productId, archivedProduct).then((product, error) => {
		if(error) {
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
};


// Controllers for activating a product
module.exports.activateProduct = (req, res) => {
	let activatedProduct = {
		isActive: true
	}
	return Product.findByIdAndUpdate(req.params.productId, activatedProduct).then((product, error) => {
		if(error) {
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
};


module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};