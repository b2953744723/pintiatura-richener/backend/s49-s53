// Dependencies and Modules
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");



// Controller to add, update, or remove a product from the user's cart
module.exports.addToCart = async (req, res) => {
  try {
    // Extract necessary information from the request
    const { productId, quantity, action } = req.body;

    // Find the product in the mock database
    const product = db.products.find((p) => p.id === productId);
    if (!product) {
      return res.send(false);
    }

    // Find the cart item corresponding to the product
    const cartItem = db.cart.find((item) => item.productId === productId);

    if (action === "add") {
      // If action is "add", add the product to the cart or update the quantity
      if (cartItem) {
        cartItem.quantity += quantity;
      } else {
        db.cart.push({
          productId,
          quantity,
          price: product.price,
        });
      }
    } else if (action === "update") {
      // If action is "update", update the quantity of the cart item
      if (cartItem) {
        cartItem.quantity = quantity;
      }
    } else if (action === "remove") {
      // If action is "remove", remove the product from the cart
      if (cartItem) {
        const index = db.cart.indexOf(cartItem);
        db.cart.splice(index, 1);
      }
    }

    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.send(false);
  }
};

// Controller to calculate the subtotal for each item and total price for all items in the cart
module.exports.calculateCartTotal = async (req, res) => {
  try {
    let subtotal = 0;

    // Calculate the subtotal for each item and total price for all items in the cart
    db.cart.forEach((item) => {
      const product = db.products.find((p) => p.id === item.productId);
      if (product) {
        const itemSubtotal = item.quantity * product.price;
        item.subtotal = itemSubtotal;
        subtotal += itemSubtotal;
      }
    });

    const total = subtotal;

    return res.json({ cart: db.cart, subtotal, total });
  } catch (error) {
    console.error(error);
    return res.send(false);
  }
};

