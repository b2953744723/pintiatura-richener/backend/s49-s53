// Mongoose Dependency
const mongoose = require('mongoose');

// Product Model

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Name is required.']
	},
	description: {
		type: String,
		required: [true, 'Description is required.']
	},
	price: {
		type: Number,
		required: [true, 'Product price is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});


module.exports = mongoose.model('Product', productSchema);